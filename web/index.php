<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../../../vendor/autoload.php';
require __DIR__ . '/../../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../../../common/config/bootstrap.php';
require __DIR__ . '/../../hrm/config/bootstrap.php';
$config = yii\helpers\ArrayHelper::merge(
	require __DIR__ . '/../../../common/config/main.php',
	require __DIR__ . '/../../../common/config/main-local.php',
	require __DIR__ . '/../../hrm/config/main.php',
	require __DIR__ . '/../../hrm/config/main-local.php',
	[
		'name' => 'Runtime Putra Pacitan',
		'components' => [
			'request' => [
				// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
				'cookieValidationKey' => 'Runtime-HRM-Solution-PutraPacitan',
			],
			'db'=>[
				'class'=>'yii\db\Connection',
				'dsn'=>'mysql:host=localhost;dbname=hrm_backend',
				'username'=>'root',
				'password'=>'runtime',
				'charset'=>'utf8',
			],
		],
		'params' => [
			'nama-inisial' => 'R',
			'upload-folder' => 'rnt',
			'pphOptions' => [
				'kd-gaji-pph21' => 'pph21',
				'kd-gaji-tunjangan-pph21' => 't_pph21',
			],
			'BPJSTenagaKerjaOptions' => [
				'premi-jkk' => 1.74 / 100,
			],
			'absenOptions' => [
				'toleransi-terlambat' => 10,
			],
		]
	],
);

(new yii\web\Application($config))->run();