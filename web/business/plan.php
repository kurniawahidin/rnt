<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Business Plan</title>
	<meta content="" name="description">
	<meta content="" name="keywords">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="assets/vendor/aos/aos.css" rel="stylesheet">
	<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
	<link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
	<link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
	<link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
	<link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

	<!-- Template Main CSS File -->
	<link href="assets/css/style.css" rel="stylesheet">

	<!-- =======================================================
  * Template Name: Arsha - v4.6.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

	<!-- ======= Header ======= -->
	<header id="header" class="fixed-top ">
		<div class="container d-flex align-items-center">

			<h1 class="logo me-auto"><a href="plan.html">Business Plan</a></h1>
			<!-- Uncomment below if you prefer to use an image logo -->
			<!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

			<nav id="navbar" class="navbar">
				<ul>
					<li><a class="nav-link scrollto active" href="#hero">Home</a></li>
					<li><a href="#harga" class="nav-link scrollto">Harga</a></li>
					<li><a href="#contribution" class="nav-link scrollto">Ambil Porsi</a></li>
					<?php /* <li><a href="#bagi-hasil" class="nav-link scrollto">Bagi Hasil</a></li> */ ?>
					<li><a href="#faq" class="nav-link scrollto">FAQ</a></li>
					<li><a href="#perhatian" class="nav-link scrollto">Perhatian</a></li>
				</ul>
				<i class="bi bi-list mobile-nav-toggle"></i>
			</nav><!-- .navbar -->

		</div>
	</header><!-- End Header -->

	<!-- ======= Hero Section ======= -->
	<section id="hero" class="d-flex align-items-center">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
					<h1>Usulan Strategi Bisnis</h1>
					<h2>Payroll System / HRIS</h2>
				</div>
				<div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
					<img src="assets/img/hero-img.png" class="img-fluid animated" alt="">
				</div>
			</div>
		</div>

	</section><!-- End Hero -->

	<main id="main">

		<!-- ======= Harga ======= -->
		<section id="harga" class="pricing section-bg">
			<div class="container" data-aos="fade-up">
				<div class="section-title">
					<h2>Harga</h2>
					<p>*) Usulan skema harga<br>**) Skema harga berikut dapat disepakati sesuai dengan strategi
						marketing.</p>
				</div>
				<div class="row">
					<div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
						<div class="box">
							<h3>0 - 200 karyawan</h3>
							<h4><sup>Rp</sup>20.000<span>per karyawan, per bulan</span></h4>
							<a href="#" class="buy-btn">Get Started</a>
						</div>
					</div>

					<div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
						<div class="box featured">
							<h3>200 - 500 karyawan</h3>
							<h4><sup>Rp</sup>17.000<span>per karyawan, per bulan</span></h4>
							<a href="#" class="buy-btn">Get Started</a>
						</div>
					</div>

					<div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
						<div class="box">
							<h3>> 500 karyawan</h3>
							<h4><sup>Rp</sup>15.000<span>per karyawan, per bulan</span></h4>
							<a href="#" class="buy-btn">Get Started</a>
						</div>
					</div>
				</div>
			</div>
		</section><!-- End Pricing Section -->

		<!-- ======= Pembagian Tugas ======= -->
		<section id="contribution" class="why-us contribution">
			<div class="container" data-aos="fade-up">
				<div class="section-title">
					<h2>Ambil Porsi</h2>
					<p>
						Sebagaimana bisnis yang bergerak dibidang layanan (<em>services</em>) keberlangsungan dan
						kualitas layanan adalah hal sangat penting.<br>
						Dalam hal ini saya perlu menarik <em>concern</em> tentang <b><em>who serve a client</em></b>.
					</p>
					<p>
						Oleh karena itu, saya menawarkan diri untuk mengambil porsi tugas teknis,
						secara detail sebagai berikut:
					</p>
				</div>
			</div>
			<div class="container" data-aos="fade-up">
				<div class="row">
					<div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch order-2 order-lg-1">
						<div class="content">
							<h3><strong>Hosting / VPS / Server</strong> dan <strong>Domain*)</strong></h3>
							<p>Saya siap menyediakan / mencari dan bekerja sama dengan penyedia layanan <strong>Hosting
									/ VPS / Dedicated Server</strong> yang tentunya sesuai dengan jumlah kebutuhan
								(client / karyawan) yang menggunakan layanan Payroll Service ini</p>
						</div>
						<div class="accordion-list">
							<ul>
								<li>
									<a class="collapse" data-bs-toggle="collapse" data-bs-target="#hosting">
										<span>01</span> Hosting / Shared Hosting
										<i class="bx bx-chevron-down icon-show"></i>
										<i class="bx bx-chevron-up icon-close"></i>
									</a>
									<div id="hosting" class="collapse show" data-bs-parent=".accordion-list">
										<p>
											Untuk keperluan demo / live demo dan selama jumlah total karyawan yang
											menggunakan layanan payroll masih sedikit (misal < 500 orang) maka untuk sementara akan menggunakan layanan shared hosting guna mengefisienkan penggunaan biaya. </p>
									</div>
								</li>
								<li>
									<a data-bs-toggle="collapse" data-bs-target="#vps" class="collapsed">
										<span>02</span> VPS / Dedicated Server
										<i class="bx bx-chevron-down icon-show"></i>
										<i class="bx-bx-chevron-up icon-close"></i>
									</a>
									<div id="vps" class="collapse" data-bs-parent=".accordion-list">
										Jika pengguna Payroll Service semakin banyak, maka akan segera diupgrade ke
										layanan VPS ataupun Dedicated Server
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style="background-image: url('assets/img/why-us.png');" data-aos="zoom-in" data-aos-delay="150">
						&nbsp;</div>
				</div>
			</div>
			<div class="container" data-aos="fade-up">
				<div class="row">
					<div class="col-lg-5 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
						<img src="assets/img/hep-desk.webp" class="img-fluid" alt="">
					</div>
					<div class="col-lg-7 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
						<h3><strong>Help Desk</strong> <em>(Operator Support)</em></h3>
						<p><i>Goal</i>-nya adalah layanan ini dapat dioperasikan oleh operator tanpa ada kendala. </p>
						<p>
							Untuk mewujudkan hal tersebut secara bertahap akan kami siapkan mulai dari <i>Online Manual Book</i>,
							berbagai tutorial sesuai dengan <i>case - case</i> yang terjadi, hingga kedepan akan kami siapkan <i>call center</i>
							yang dapat mensupport kendala - kendala dalam pengoperasian.
						</p>
						<p>
							Tentunya dengan mempertimbangkan jumlah client yang menggunakan layanan ini.
						</p>
					</div>
				</div>
			</div>
			<div class="container" data-aos="fade-up">
				<div class="row">
					<div class="col-lg-7 d-flex flex-column align-items-stretch order-2 order-lg-1 content" data-aos="fade-right" data-aos-delay="100">
						<h3><strong>Programming Support</strong></h3>
						<p>Tidak ada satupun sistem komputer yang <i>bugs free</i>, bagaimanapun itu kita hanya bisa meminimalisirnya.</p>
						<p>Terlebih jika ada <i>requirement</i> yang memang harus terjadi perubahan, (misal: perubahan regulasi dari pemerintah)</p>
						<p>Oleh karena itu kebutuhan terkait <i>programming / programmer</i> secara terus menerus tidak dapat dihindarkan, untuk layanan berbasis IT seperti ini</p>
					</div>
					<div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" data-aos="fade-right" data-aos-delay="100">
						<img src="assets/img/programmer.png" class="img-fluid" alt="">
					</div>
				</div>
			</div>
		</section>

		<?php /*
		<!-- ======= Bagi Hasil ======= -->
		<section id="bagi-hasil" class="pricing section-bg">
			<div class="container" data-aos="fade-up">
				<div class="section-title">
					<h2>Usulan Metode Bagi Hasil</h2>
				</div>
			</div>
			<div class="container" data-aos="fade-up">
				<div class="row">
					<div class="col-lg-6 d-flex flex-column">
						<div class="content">
							<p>
								Bagaimanapun juga tujuan akhir dari sebuah bisnis adalah untuk mendapatkan keuntungan.
								Begitupun bisnis yang dilakukan dengan melibatkan banyak kerja sama berbagai pihak yang
								masing - masing mengambil peran,
								perlu disepakati tentang perhitungan bagi hasilnya.
							</p>
							<h5>Sesuai dengan porsi peran yang siap saya handle, sebagai berikut;</h5>
						</div>
						<ol>
							<li>Hosting / VPS / Server</li>
							<li>Help Desk <em>(Operator Support)</em></li>
							<li>Programming Support</li>
						</ol>
					</div>
					<div class="col-lg-6" data-aos="zoom-in" data-aos-delay="500">
						<div class="box featured">
							<h3>Saya mengusulkan bagihasil</h3>
							<h4><sup>Rp</sup>7.000<span>per karyawan, per bulan</span></h4>
							<ul>
								<li><i class="bx bx-check"></i> Dasar perhitungan bruto (sesuai dengan jumlah karyawan)
								</li>
								<li><i class="bx bx-check"></i> Bagi hasil mudah dihitung</li>
								<li><i class="bx bx-check"></i> Semua biaya yang timbul dari porsi peran yang saya
									ambil, akan menjadi tanggung jawab saya.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		*/ ?>

		<!-- ======= Cta Section ======= -->
		<section id="faq" class="cta">
			<div class="container" data-aos="zoom-in">
				<div class="row">
					<div class="col-lg-9 text-center text-lg-start">
						<h3>Kebebasan Tim Marketing</h3>
						<p>
							Pada dasarnya tidak ada pembatasan kreativitas untuk team marketing dalam menjalankan tugas
							nya.
							Namun mungkin ada beberapa hal yang perlu ditegaskan atau digaris bawahi, karena beberapa
							diantaranya bisa jadi akan menimbulkan konsekwensi di tim yg lain.
						</p>
					</div>
					<div class="col-lg-3 cta-btn-container text-center">
						<a class="cta-btn align-middle scrollto" href="#faq">Call To Action</a>
					</div>
				</div>
			</div>
		</section>
		<!-- End Cta Section -->

		<!-- ======= Kebebasan Marketing ======= -->
		<section id="faq" class="faq section-bg">
			<div class="container" data-aos="fade-up">
				<div class="section-title">
					<h2>Kebebasan Tim Marketing</h2>
				</div>
				<div class="faq-list">
					<ul>
						<li data-aos="fade-up" data-aos-delay="100">
							<i class="bx bx-help-circle icon-help"></i>
							<a class="collapse" data-bs-toggle="collapse" data-bs-target="#faq-list-harga">
								Penentuan skema harga jual & perubahan harga jual
								<i class="bx bx-chevron-down icon-show"></i>
								<i class="bx bx-chevron-up icon-close"></i>
							</a>
							<div id="faq-list-harga" class="collapse show" data-bs-parent=".faq-list">
								<p>
									Skema harga jual dan perubahan harga jual menjadi kewengangan penuh team marketing /
									management marketing.
									Dengan mempertimbangkan kesepakatan bagihasil untuk tim <em>backend technical
										support</em>.
								</p>
							</div>
						</li>
						<li data-aos="fade-up" data-aos-delay="200">
							<i class="bx bx-help-circle icon-help"></i>
							<a class="collapse" data-bs-toggle="collapse" data-bs-target="#faq-list-gratis">
								Coba Gratis, dan Promo Gratis lainnya
								<i class="bx bx-chevron-down icon-show"></i>
								<i class="bx bx-chevron-up icon-close"></i>
							</a>
							<div id="faq-list-gratis" class="collapse" data-bs-parent=".faq-list">
								<p>
									Begitupun, strategi promosi <b>Coba Gratis</b>, dan <b>Promo Gratis</b> lainnya
									adalah kewenangan penuh team marketing / management marketing,
									namun hal yang perlu dipertimbangkan adalah berapa dan bagaimana mekanisme
									kompensasi strategi promosi tersebut terhadap <i>effort</i>
									yang dilakukan oleh <i>backend technical support</i>.
								</p>
								<p>
									Acuan dasar saya adalah sesuai dengan usulan bagi hasil yang telah saya ajukan
									diatas.
								</p>
							</div>
						</li>
						<li data-aos="fade-up" data-aos-delay="300">
							<i class="bx bx-help-circle icon-help"></i>
							<a class="collapse" data-bs-toggle="collapse" data-bs-target="#faq-list-discount">
								Promo Discount Harga
								<i class="bx bx-chevron-down icon-show"></i>
								<i class="bx bx-chevron-up icon-close"></i>
							</a>
							<div id="faq-list-discount" class="collapse" data-bs-parent=".faq-list">
								<p>
									Discount harga menjadi kewengangan penuh team marketing / management marketing,
									dengan mempertimbangkan kesepakatan bagihasil untuk tim <em>backend technical
										support</em> ataupun kompensasinya.
								</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</section>

		<!-- ======= Hal - hal Perhatikan ======= -->
		<section id="perhatian" class="why-us">
			<div class="container-fluid" data-aos="fade-up">
				<div class="section-title">
					<h2>Hal - Hal Yang Mungkin Perlu Mendapatkan Perhatian</h2>
				</div>
				<div class="row">
					<div class="col-lg-6 content" data-aos="fade-right">
						<h3><b>Legalitas</b> dan <b>HAKI</b></h3>
						<p>
							<b>Legalitas</b>, bisa jadi bukan prioritas saat ini. Namun seiring dengan semakin
							berkembangnya usaha kerja sama ini dikemudian hari, jangan sampai legalitas ini menjadi
							kendala.
						</p>
						<p>
							Begitupun dengan <b>HAKI</b> (Hak Kekayaan Intelektual), jangan sampai identias (misal
							brand/nama layanan) di claim oleh pihak lain karena kita tidak mempersiapkan diri untuk itu.
						</p>
					</div>
					<div class="col-lg-6" data-aos="fade-left" data-aos-delay="100">
						<img src="assets/img/legal.png" class="img-fluid" alt="">
						<img src="assets/img/copyright.png" alt="" class="img-fluid" style="height: 300px;">
					</div>
				</div>
			</div>
		</section>
		<section id="skills" class="skills">
			<div class="container" data-aos="fade-up">
				<div class="row">
					<div class="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
						<img src="assets/img/skills.png" class="img-fluid" alt="">
					</div>
					<div class="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
						<h3>Pengembangan System Berkelanjutan</h3>
						<p>
							Untuk memberikan layanan terbaik bagi client dan guna mempertahankan daya saing dengan kompetitor, perlu adanya pengembangan secara berkelanjutan.
							Meskipun ini bukan <i>concern</i> utama namun hal ini tidak boleh diabaikan begitu saja.
						</p>
					</div>
				</div>
			</div>
		</section>











	</main><!-- End #main -->

	<!-- ======= Footer ======= -->
	<footer id="footer">
	</footer>
	<!-- End Footer -->

	<div id="preloader"></div>
	<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

	<!-- Vendor JS Files -->
	<script src="assets/vendor/aos/aos.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
	<script src="assets/vendor/waypoints/noframework.waypoints.js"></script>

	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>

</body>

</html>